<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Twitt;
use Illuminate\Http\Request;
use Session;
use DB;
use App\Quotation;

class TwittsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index() {
        //$twitts = Twitt::orderBy('created_at', 'desc')->paginate(25);
        
        $twitts = DB::table('twitts')
                        ->join('users', 'users.id', '=', 'twitts.id_user')
                        ->orderBy('twitts.created_at', 'desc')
                        ->take(10)
                        ->get(array('twitts.id_user', 'users.image', 'twitts.body', 'twitts.nama', 'twitts.created_at'));

        return view('twitts.index', compact('twitts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('twitts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {

        $requestData = $request->all();

        Twitt::create($requestData);

        Session::flash('flash_message', 'Twitt added!');

        return redirect('twitts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $twitt = Twitt::findOrFail($id);

        return view('twitts.show', compact('twitt'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $twitt = Twitt::findOrFail($id);

        return view('twitts.edit', compact('twitt'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request) {

        $requestData = $request->all();

        $twitt = Twitt::findOrFail($id);
        $twitt->update($requestData);

        Session::flash('flash_message', 'Twitt updated!');

        return redirect('twitts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        Twitt::destroy($id);

        Session::flash('flash_message', 'Twitt deleted!');

        return redirect('twitts');
    }

}
