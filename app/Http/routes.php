<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Route::resource('user', 'userController');
Route::auth();

Route::get('/home', 'HomeController@index');

Route::resource('positions', 'PositionsController');
Route::resource('departments', 'DepartmentsController');
Route::resource('employees_experiences', 'employees_experiencesController');
Route::resource('employees_experiences', 'employees_experiencesController');
Route::resource('employees_educations', 'employees_educationsController');
Route::resource('employees', 'employeesController');
Route::resource('twitts', 'TwittsController');