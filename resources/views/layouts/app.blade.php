<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Twitter Application</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
        <!-- Styles -->
        <link href="{{ URL::asset('bootstrap/css/bootstrap.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ URL::asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
        <link id="bsdp-css" href="{{ URL::asset('bootstrap-datepicker/bootstrap-datepicker3.min.css') }}" rel="stylesheet">
        {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

        <style>
            body {
                font-family: 'Lato';
            }

            .fa-btn {
                margin-right: 6px;
            }
        </style>
    </head>
    <body id="app-layout">
        <nav class="navbar navbar-twitter navbar-static-top">
            <div class="container">

                <div class="navbar-header">
                    <a class="navbar-brand" href="{{ url('/') }}">
                        Twitter Application
                    </a>
                </div>
            </div>
        </nav>

        @yield('content')
        <!-- JavaScripts -->
        <script type="text/javascript" src="{{ URL::asset('jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('bootstrap/js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('bootstrap-datepicker/bootstrap-datepicker3.min.js')}}"></script>
        {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}

</body>
</html>
