@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class='panel panel-default'>
                <div class="panel-body">
                    <img src="{{asset('uploads/'.Auth::user()->image) }}" class="img-circle" alt="{{Auth::user()->name}}" width="60px" height="60px"> 
                    {{ Auth::user()->name }}<br/>
                    <a href="{{ url('/user/'.Auth::user()->id.'/edit') }}"><i class="fa fa-btn fa-gear"></i>Setting</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="{{ url('/logout') }}"></i>Logout</a>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-lg-12">
                        {!! Form::open(['url' => '/twitts', 'class' => 'form-horizontal', 'files' => true]) !!}
                        <div class="form-group">
                            {!! Form::hidden('id_user', Auth::user()->id, ['class' => 'form-control']) !!}
                            {!! Form::hidden('nama', Auth::user()->name, ['class' => 'form-control']) !!}
                            {!! Form::text('body', null, ['class' => 'form-control', 'placeholder' => 'Update Status..', 'rows'=>'1']) !!}
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-11">
                                <button type="submit" name="button" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Twitts -->

    <ul class="timeline">
        @foreach($twitts as $item)
        @if ($item->id_user == Auth::user()->id)
        <li class="timeline-inverted">
            <div class="timeline-badge">
                <img src="{{asset('uploads/'.$item->image) }}" class="img-circle" alt="" width="50px" height="50px"> 
            </div>
            <div class="timeline-panel">
                <div class="timeline-heading">
                    <h4 class="timeline-title">Saya</h4>
                    <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> {{date('d M Y H:i:s', strtotime($item->created_at))}}</small></p>
                </div>
                <div class="timeline-body">
                    <p>{{$item->body}}</p>
                </div>
            </div>
        </li>
        @else
        <li>
            <div class="timeline-badge">
                <img src="{{asset('uploads/'.$item->image) }}" class="img-circle" alt="" width="50px" height="50px">
            </div>
            <div class="timeline-panel">
                <div class="timeline-heading">
                    <h4 class="timeline-title">{{$item->nama}}</h4>
                    <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> {{date('d M Y H:i:s', strtotime($item->created_at))}}</small></p>
                </div>
                <div class="timeline-body">
                    <p>{{$item->body}}</p>
                </div>
            </div>
        </li>
        @endif

        @endforeach
    </ul>

    <!-- Twitts End -->
</div>


@endsection