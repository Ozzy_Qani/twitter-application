<div class=" col-md-12 col-lg-12 "> 
    <table class="table table-user-information">
        <tbody>
            <tr>
                <td rowspan="3" align='center'>
                    <img alt="User Pic" src="{{asset('uploads/'.Auth::user()->image) }}" class="img-circle" width="120px" height="120px"> <br/>
                </td> 
                <td>Name:</td>
                <td>
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </td>
            </tr>
            <tr>
                <td>Email:</td>
                <td>
                    {!! Form::text('email', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                </td>
            </tr>
            <tr>
                <td>Password :</td>
                <td>
                    {!! Form::text('password', null, ['class'=>'form-control pw']) !!}
                    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                    {!! Form::hidden('remamber_token', 'secret', ['class' => 'form-control']) !!}
                </td>
            </tr>
            <tr>
                <td> 
                    {!! Form::file('image', ['class' => 'btn btn-sm btn-default']) !!}
                </td>
                <td>{!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}</td>
            </tr>
        </tbody>
    </table>
</div>