@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class='panel panel-default'>
                <div class="panel-body">
                    <ul class="nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }}
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-lg-12">
                        {!! Form::open(['url' => '/twitts', 'class' => 'form-horizontal', 'files' => true]) !!}
                        <div class="form-group">
                            {!! Form::hidden('id_user', Auth::user()->name, ['class' => 'form-control']) !!}
                            {!! Form::textarea('body', null, ['class' => 'form-control', 'placeholder' => 'Update Status..', 'rows'=>'1']) !!}
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-11">
                                <button type="submit" name="button" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
